/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define PS 1000
#define PL 500
#define PM 250
#define PR 50
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
int get_modo_oper(void);
int get_seq_oper(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();

	/* Initialize interrupts */
	MX_NVIC_Init();
	/* USER CODE BEGIN 2 */
	static enum {INI_D1, LIG_D1, DSLG_D1} sttD1=INI_D1; // var estados de D1
	static enum {INI_D2, LIG_D2, DSLG_D2} sttD2=INI_D2; // var estados de D2
	static enum {INI_D3, LIG_D3, DSLG_D3} sttD3=INI_D3; // var estados de D3
	static enum {INI_D4, LIG_D4, DSLG_D4} sttD4=INI_D4; // var estados de D4

	uint32_t tin_D1=0, tin_D2=0, tin_D3=0, tin_D4=0;
	uint32_t dt_DN[4] = {0,0,0,0}; // proximos dt p/ cada LED

	int modo = get_modo_oper();
	int seq = get_seq_oper();
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
		modo = get_modo_oper();
		seq = get_seq_oper();
		switch(modo)
		{
		case 0: // mo modo '0' ajusta
			dt_DN[(-seq)*0+(seq+1)*3] = PS;
			dt_DN[(-seq)*1+(seq+1)*2] = PL;
			dt_DN[(-seq)*2+(seq+1)*1] = PM;
			dt_DN[(-seq)*3+(seq+1)*0] = PR;
			break;
		case 1: // mo modo '1' reajusta dt_xx dos LEDs
			dt_DN[(-seq)*0+(seq+1)*3] = PL;
			dt_DN[(-seq)*1+(seq+1)*2] = PM;
			dt_DN[(-seq)*2+(seq+1)*1] = PR;
			dt_DN[(-seq)*3+(seq+1)*0] = PS;
			break;
		case 2: // mo modo '2' reajusta dt_xx dos LEDs
			dt_DN[(-seq)*0+(seq+1)*3] = PM;
			dt_DN[(-seq)*1+(seq+1)*2] = PR;
			dt_DN[(-seq)*2+(seq+1)*1] = PS;
			dt_DN[(-seq)*3+(seq+1)*0] = PL;
			break;
		case 3: // mo modo '3' reajusta dt_xx dos LEDs
			dt_DN[(-seq)*0+(seq+1)*3] = PR;
			dt_DN[(-seq)*1+(seq+1)*2] = PS;
			dt_DN[(-seq)*2+(seq+1)*1] = PL;
			dt_DN[(-seq)*3+(seq+1)*0] = PM;
			break;
		}

		switch (sttD1) {
		case INI_D1:
			tin_D1 = HAL_GetTick();
			sttD1 = LIG_D1;
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET); // desliga o LED break;
		case LIG_D1: // estado para ligar o LED
			if((HAL_GetTick()-tin_D1)>dt_DN[0]) // se HAL_GetTick()-tin_D1 > dt_DN[0]
			{
				tin_D1 = HAL_GetTick(); // guarda tempo p/ prox mudança estado
				sttD1 = DSLG_D1; // muda o prox estado da máquina
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET); // liga o LED
			}
			break;
		case DSLG_D1: // estado para desligar o LED
			if((HAL_GetTick()-tin_D1)>dt_DN[0]) // se HAL_GetTick()-tin_D1 > dt_DN[0]
			{
				tin_D1 = HAL_GetTick(); // guarda tempo p/ prox mudança estado
				sttD1 = LIG_D1; // muda o prox estado da máquina
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET); // desliga o LED
			}
			break;
		};
		switch (sttD2) {
		case INI_D2:
			tin_D2 = HAL_GetTick();
			sttD2 = LIG_D2;
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET); // desliga o LED break;
		case LIG_D2: // estado para ligar o LED
			if((HAL_GetTick()-tin_D2)>dt_DN[1]) // se HAL_GetTick()-tin_D2 > dt_DN[1]
			{
				tin_D2 = HAL_GetTick(); // guarda tempo p/ prox mudança estado
				sttD2 = DSLG_D2; // muda o prox estado da máquina
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET); // liga o LED
			}
			break;
		case DSLG_D2: // estado para desligar o LED
			if((HAL_GetTick()-tin_D2)>dt_DN[1]) // se HAL_GetTick()-tin_D2 > dt_DN[1]
			{
				tin_D2 = HAL_GetTick(); // guarda tempo p/ prox mudança estado
				sttD2 = LIG_D2; // muda o prox estado da máquina
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET); // desliga o LED
			}
			break;
		};
		switch (sttD3) {
		case INI_D3:
			tin_D3 = HAL_GetTick(); sttD3 = LIG_D3;
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET); // desliga o LED
			break;
		case LIG_D3: // estado para ligar o LED
			if((HAL_GetTick()-tin_D3)>dt_DN[2]) // se HAL_GetTick()-tin_D3 > dt_DN[2]
			{
				tin_D3 = HAL_GetTick(); // guarda tempo p/ prox mudança estado
				sttD3 = DSLG_D3; // muda o prox estado da máquina
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET); // liga o LED
			}
			break;
		case DSLG_D3: // estado para desligar o LED
			if((HAL_GetTick()-tin_D3)>dt_DN[2]) // se HAL_GetTick()-tin_D3 > dt_DN[2]
			{
				tin_D3 = HAL_GetTick(); // guarda tempo p/ prox mudança estado
				sttD3 = LIG_D3; // muda o prox estado da máquina
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET); // desliga o LED
			}
			break;
		};
		switch (sttD4) {
		case INI_D4:
			tin_D4 = HAL_GetTick();
			sttD4 = LIG_D4;
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET); // desliga o LED break;
		case LIG_D4: // estado para ligar o LED
			if((HAL_GetTick()-tin_D4)>dt_DN[3]) // se HAL_GetTick()-tin_D4 > dt_DN[3]
			{
				tin_D4 = HAL_GetTick(); // guarda tempo p/ prox mudança estado
				sttD4 = DSLG_D4; // muda o prox estado da máquina
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET); // liga o LED
			}
			break;
		case DSLG_D4: // estado para desligar o LED
			if((HAL_GetTick()-tin_D4)>dt_DN[3]) // se HAL_GetTick()-tin_D4 > dt_DN[3]
			{
				tin_D4 = HAL_GetTick(); // guarda tempo p/ prox mudança estado
				sttD4 = LIG_D4; // muda o prox estado da máquina
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET); // desliga o LED
			}
			break;
		};
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		Error_Handler();
	}
}

/**
 * @brief NVIC Configuration.
 * @retval None
 */
static void MX_NVIC_Init(void)
{
	/* EXTI1_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	/* EXTI2_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);
	/* EXTI3_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

	/*Configure GPIO pins : PA1 PA2 PA3 */
	GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : PB12 PB13 PB14 PB15 */
	GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
