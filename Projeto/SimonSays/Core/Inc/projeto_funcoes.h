#ifndef PRAT_05_FUNCOES_H_
#define PRAT_05_FUNCOES_H_

#define DISP_TIPO 0x0
#define TIPO_DISPLAY 0

int16_t conv_7_seg(int NumHex);

void serializar(int ser_data);         // prot fn serializa dados p/ 74HC595

void reset_pin_GPIOs(void);

#define DIGITO_APAGADO 0x10    // kte valor p/ apagar um dígito no display
void displayShow(uint32_t value);
void displayClear(void);

uint32_t lcg(void);
int rand3(void);

void SeedSequence(uint32_t value);

#endif
