/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "mx_prat_05_funcoes.h"   // header do arqv das funcoes do programa
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define all_leds GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
int MAX_LVL = 20;
int cTrial = 1;
int level = 1;
int tdelay = 200;
int32_t max_score = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */
	uint32_t tIN_varre = 0,
			DT_VARRE = 5;
	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();

	/* Initialize interrupts */
	MX_NVIC_Init();
	/* USER CODE BEGIN 2 */
	reset_pin_GPIOs();
	HAL_GPIO_WritePin(GPIOB, all_leds|GPIO_PIN_5, 1);
	static enum {DISP_MAX, DISP_LVL} sttDisplay=DISP_MAX;
	static enum {INIT, SEQ, RUN, REP, NEXTLVL, GAMEOVER, THEEND} sttGAME=INIT;
	static enum {INI_TRIAL, RUN_TRIAL, NEXT_TRIAL} sttTrial=INI_TRIAL;
	static enum {LIG_LED, DES_LED, END_LED} sttLED=LIG_LED;
	static enum {LIG_BUZZ, DES_BUZZ, END_BUZZ} sttBUZZ=LIG_BUZZ;

	uint16_t velocity = 1000;
	uint16_t led_lights[] = {GPIO_PIN_15, GPIO_PIN_14, GPIO_PIN_13};
	uint16_t tin_LED=0, tin_BUZZ=0, tin_TRIAL=0, tin_STATE=0;
	uint16_t dt_STATE=500, dt_TRIAL=200;

	int repSequence[MAX_LVL];
	int sequence[MAX_LVL];
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		switch (sttDisplay){
		case DISP_MAX:
			if (HAL_GetTick()-tIN_varre) > DT_VARRE){    // se ++0,1s atualiza o display
				displayShow(max_score);
				tIN_varre = HAL_GetTick();           // tmp atual em que fez essa varredura
			}
		case DISP_LVL:
			if (HAL_GetTick()-tIN_varre) > DT_VARRE){    // se ++0,1s atualiza o display
				score = (uint32_t) ((float) (level - 1) / MAX_LVL * 9999);
				displayShow(score);
				tIN_varre = HAL_GetTick();           // tmp atual em que fez essa varredura
			}
		}

		switch (sttGAME){
		case INIT:
			if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)==0){
				uint8_t state = 0;
				int tdt = 0;
				while (tdt < tdelay){
					if (state == HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)){
						++ tdt;
					} else {
						state = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1);
						tdt = 0;
					}
				}

				if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)==0){
					uint8_t state = 0;
					int tdt = 0;
					while (tdt < tdelay){
						if (state == HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)){
							++ tdt;
						} else {
							state = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3);
							tdt = 0;
						}
					}
					if ((HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)==0)&(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)==0)) sttGAME = SEQ;
				}
			}

			if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)==0){
				uint8_t state = 0;
				int tdt = 0;
				while (tdt < tdelay){
					if (state == HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)){
						++ tdt;
					} else {
						state = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3);
						tdt = 0;
					}
				}

				if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)==0){
					uint8_t state = 0;
					int tdt = 0;
					while (tdt < tdelay){
						if (state == HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)){
							++ tdt;
						} else {
							state = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1);
							tdt = 0;
						}
					}
					if ((HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)==0)&(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)==0)) sttGAME = SEQ;
				}
			}
			level = 1;
			cTrial = 0;
			tin_STATE = HAL_GetTick();
			break;

		case SEQ:
			SeedSequence(HAL_GetTick());
			for(int i = 0; i < MAX_LVL; i++){
				sequence[i] = rand3();
			}
			sttGAME = RUN;

		case RUN:
			if ((HAL_GetTick()-tin_STATE)>dt_STATE) {
				switch (sttTrial){
				case INI_TRIAL:
					cTrial = 0;
					sttTrial = RUN_TRIAL;
					break;

				case RUN_TRIAL:
					switch (sttLED) {
					case LIG_LED:
						tin_LED = HAL_GetTick();
						sttLED = DES_LED;
						HAL_GPIO_WritePin(GPIOB, led_lights[sequence[cTrial]], 0);
						break;
					case DES_LED:
						if ((HAL_GetTick()-tin_LED)>velocity){
							sttLED = END_LED;
							HAL_GPIO_WritePin(GPIOB, led_lights[sequence[cTrial]], 1);
						}
						break;
					case END_LED:
						HAL_GPIO_WritePin(GPIOB, led_lights[sequence[cTrial]], 1);
						tin_TRIAL = HAL_GetTick();
						sttTrial = NEXT_TRIAL;
						break;
					}

					switch (sttBUZZ) {
					case LIG_BUZZ:
						tin_BUZZ = HAL_GetTick();
						sttBUZZ = DES_BUZZ;
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0);
						break;
					case DES_BUZZ:
						if ((HAL_GetTick()-tin_BUZZ) > velocity/5) {
							sttBUZZ = END_BUZZ;
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 1);
						}
						break;

					case END_BUZZ:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 1);
						break;
					}
					break;

					case NEXT_TRIAL:
						if ((HAL_GetTick()-tin_TRIAL)>dt_TRIAL) {
							sttLED = LIG_LED;
							sttBUZZ = LIG_BUZZ;
							if (cTrial < level-1){
								++ cTrial;
								sttTrial = RUN_TRIAL;
							} else {
								sttTrial = INI_TRIAL;
								sttGAME = REP;
								tin_STATE = HAL_GetTick();
							}
						}
						break;
				}
			}
			break;

		case REP:
			if ((HAL_GetTick()-tin_STATE)>dt_STATE) {
				for (int i = 0; i < level; i++){
					int flag = 0;
					while (flag == 0){
						if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)==0){
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, 0);
							while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)==0){}
							HAL_Delay(500);
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, 1);
							flag = 1;
							repSequence[i] = 0;
						}

						if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)==0){
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 0);
							while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)==0){}
							HAL_Delay(500);
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 1);
							flag = 1;
							repSequence[i] = 1;
						}

						if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)==0){
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, 0);
							while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)==0){}
							HAL_Delay(500);
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, 1);
							flag = 1;
							repSequence[i] = 2;
						}
					}

					if (repSequence[i] != sequence[i]) {
						sttGAME = GAMEOVER;
						break;
					} else {
						sttGAME = NEXTLVL;
					}
				}
				tin_STATE = HAL_GetTick();
			}
			break;

		case NEXTLVL:
			if ((HAL_GetTick()-tin_STATE)>dt_STATE) {
				HAL_GPIO_WritePin(GPIOB, all_leds, 0);
				HAL_Delay(500);
				HAL_GPIO_WritePin(GPIOB, all_leds, 1);
				HAL_Delay(100);
				if (level < MAX_LVL - 1){
					if (velocity >= 300) velocity = velocity - 100;
					sttGAME = RUN;
					++level;
				}

				else sttGAME = THEEND;
				tin_STATE = HAL_GetTick();
			}
			break;

		case GAMEOVER:
			if ((HAL_GetTick()-tin_STATE)>dt_STATE) {
				for (int i=0; i<3; i++){
					HAL_GPIO_WritePin(GPIOB, all_leds|GPIO_PIN_5, 0);
					HAL_Delay(500);
					HAL_GPIO_WritePin(GPIOB, all_leds|GPIO_PIN_5, 1);
					HAL_Delay(100);
				}
				sttGAME = INIT;
				max_score = (max_score < level) ? level : max_score;
				tin_STATE = HAL_GetTick();
			}
			break;

		case THEEND:
			// Mostra Placar
			max_score = level;
			sttGAME = INIT;
			tin_STATE = HAL_GetTick();
		}
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
}

/**
 * @brief NVIC Configuration.
 * @retval None
 */
static void MX_NVIC_Init(void)
{
	/* EXTI3_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
	/* EXTI2_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);
	/* EXTI1_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
			|GPIO_PIN_15|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_9, GPIO_PIN_RESET);

	/*Configure GPIO pins : PA1 PA2 PA3 */
	GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : PB10 PB12 PB13 PB14
                           PB15 PB5 PB6 PB9 */
	GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
			|GPIO_PIN_15|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
